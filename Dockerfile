FROM maven:3.6.3-jdk-8

RUN git clone https://gitlab.com/Yulia_Krawtschenko/bikerenting.git

WORKDIR bikerenting/spark-streaming/tohbase/

RUN mvn clean compile assembly:single

ENV HDP_IP=10.132.0.23
ENV HDP_DOMAIN_NAME sandbox.hortonworks.com

ENV MAIN_CLASS=streaming.Streaming
ENV MASTER=local
ENV BROKERS_TO_READ=localhost:9092
ENV TOPIC_TO_READ=avro_test
ENV BROKERS_TO_WRITE=localhost:9092
ENV TOPIC_TO_WRITE=kafka_event
ENV FORMAT=avro
ENV SCHEMA_REGISTRY_URL=http://localhost:8081
ENV CHECKPOINT_PATH=D:\EPAM\BigDataProject\checkpoint
ENV WINDOW_DURATION=5
ENV MIN_BIKES_THRESHOLD=5
ENV MIN_EBIKES_THRESHOLD=5
ENV MIN_DOCKS_THRESHOLD=5
ENV HOUR=3600000
ENV MESSAGE_DELAY=$HOUR
ENV WRITE_TO_HBASE=true
ENV ZOOKEEPER_QUORUM=localhost
ENV NAMESPACE=bikes
ENV TABLE_NAME_SII=station_status_info
ENV FAMILY_NAME_SII=data
ENV TABLE_NAME_KE=kafka_event
ENV FAMILY_NAME_KE=data
ENV BUTCH_SIZE=50

CMD echo "$HDP_IP sandbox-hdp.hortonworks.com" >> /etc/hosts && spark-submit --class $MAIN_CLASS --master $MASTER target/tohbase-1.0.0-jar-with-dependencies.jar \
--master $MASTER --brokers-to-read $BROKERS_TO_READ --topic-to-read $TOPIC_TO_READ --brokers-to-write $BROKERS_TO_WRITE --topic-to-write $TOPIC_TO_WRITE \
--format $FORMAT --schema-registry-url $SCHEMA_REGISTRY_URL --checkpoint-path $CHECKPOINT_PATH --window-duration $WWINDOW_DURATION \
--min-bikes-threshold $MIN_BIKES_THRESHOLD --min-ebikes-threshold $MIN_EBIKES_THRESHOLD --min-docks-threshold $MIN_DOCKS_THRESHOLD \
--message-delay $MESSAGE_DELAY --write-to-hbase $WRITE_TO_HBASE --zookeeper-quorum $ZOOKEEPER_QUORUM --namespace $NAMESPACE \
--table-name-ssi $TABLE_NAME_SII --family-name-ssi $FAMILY_NAME_SII --table-name-ke $TABLE_NAME_KE --family-name-ke $FAMILY_NAME_KE \
--butch-size $BUTCH_SIZE


