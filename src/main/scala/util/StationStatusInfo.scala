package util


import org.apache.hadoop.hbase.util.Bytes
import org.apache.spark.sql.types.{IntegerType, LongType, StringType, StructField, StructType}


case class StationStatusInfo(stationId: String, numBikesAvailable: Int,
                             numDocksAvailable: Int, lastReported: Long,
                             numEbikesAvailable: Int, stationStatus: String)

object StationStatusInfo {
    
    val schema = new StructType()
        .add("stationId", StringType)
        .add("numBikesAvailable", IntegerType)
        .add("numDocksAvailable", IntegerType)
        .add("lastReported", LongType)
        .add("numEbikesAvailable", IntegerType)
        .add("stationStatus", StringType)

    val tableColumnName: List[String] = List(
        "stationId",
        "numBikesAvailable",
        "numDocksAvailable",
        "lastReported",
        "numEbikesAvailable",
        "stationStatus"
    )
    
    def getSchema: StructType = {
        schema
    }
    
    def createFromMap(map: Map[String, Any]): StationStatusInfo = {
        StationStatusInfo (
            map(tableColumnName(0)).asInstanceOf[String],
            map(tableColumnName(1)).asInstanceOf[Int],
            map(tableColumnName(2)).asInstanceOf[Int],
            map(tableColumnName(3)).asInstanceOf[Long],
            map(tableColumnName(4)).asInstanceOf[Int],
            map(tableColumnName(5)).asInstanceOf[String]
        )
    }
    
    def createFromMapBytesArray(map: Map[String, Array[Byte]]): StationStatusInfo = {
        StationStatusInfo (
            Bytes.toString(map(tableColumnName(0))),
            Bytes.toInt(map(tableColumnName(1))),
            Bytes.toInt(map(tableColumnName(2))),
            Bytes.toLong(map(tableColumnName(3))),
            Bytes.toInt(map(tableColumnName(4))),
            Bytes.toString(map(tableColumnName(5)))
        )
    }
    
    def getKey(model: StationStatusInfo): String ={
        model.stationId.toString.concat("-").
            concat(model.lastReported.toString)
    }
}



