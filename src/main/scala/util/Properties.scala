package util

import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.hbase.HBaseConfiguration

object Properties {
    //SDM - streamingDataModel
    //KE - kafka event
    
    var MASTER = "local"
    
    var WRITE_TO_HBASE = true

    var ZOOKEEPER_QUORUM = "localhost"
    var NAMESPACE = "test_namespace"

    var FAMILY_NAME_SDM = "first_family"
    var TABLE_NAME_SDM = "streaming_data"
    var FAMILY_NAME_KE = "first_family"
    var TABLE_NAME_KE = "kafka_event"

    var HBASE_CONFIG : Configuration = HBaseConfiguration.create()
    HBASE_CONFIG.set("hbase.zookeeper.quorum", ZOOKEEPER_QUORUM)
    
    
    var MIN_BIKES_THRESHOLD = 5
    var MIN_EBIKES_THRESHOLD = 5
    var MIN_DOCKS_THRESHOLD = 5
    var WINDOW_DURATION = 5//in minutes
    
    var HOSTS_PORTS_TO_WRITE = "localhost:9092"
    var HOSTS_PORTS_TO_READ = "localhost:9092"
    var CHECKPOINT_TO_WRITE = "D:\\EPAM\\BigDataProject\\checkpoint"
    var TOPIC_TO_WRITE = "stk" //"stringreport"
    var TOPIC_TO_READ = "bikes" //"bikes"
    
    var MESSAGE_DELAY = 3600*1000 //1 hour
    
    var BUTCH_SIZE_TO_WRITE_HBASE = 50
    
    var INPUT_FORMAT = "avro"
    var SCHEMA_REGISTRY_ADDRESS = "http://localhost:8081"
    //var AVRO_SCHEMA_FILE_PARH = "src/main/resources/StationStatusInfo.avsc"
    
    def setProperties(args: Array[String]): Unit = {
        
        args.sliding(2, 2).toList.collect {
            case Array("--master", value: String) => Properties.MASTER = value
            case Array("--brokers-to-read", value: String) => Properties.HOSTS_PORTS_TO_READ = value
            case Array("--topic-to-read", value: String) => Properties.TOPIC_TO_READ = value
            case Array("--brokers-to-write", value: String) => Properties.HOSTS_PORTS_TO_WRITE = value
            case Array("--topic-to-write", value: String) => Properties.TOPIC_TO_WRITE = value
            case Array("--format", value: String) => Properties.INPUT_FORMAT = value
            case Array("--schema-registry-url", value: String) => Properties.SCHEMA_REGISTRY_ADDRESS = value
            //case Array("--schema-path", value: String) => Properties.AVRO_SCHEMA_FILE_PARH = value
            case Array("--checkpoint-path", value: String) => Properties.CHECKPOINT_TO_WRITE = value
            case Array("--window-duration", value: String) => Properties.WINDOW_DURATION = value.toInt
            case Array("--min-bikes-threshold", value: String) => Properties.MIN_BIKES_THRESHOLD = value.toInt
            case Array("--min-ebikes-threshold", value: String) => Properties.MIN_EBIKES_THRESHOLD = value.toInt
            case Array("--min-docks-threshold", value: String) => Properties.MIN_DOCKS_THRESHOLD = value.toInt
            case Array("--message-delay", value: String) => Properties.MESSAGE_DELAY = value.toInt
            case Array("--write-to-hbase", value: String) => Properties.WRITE_TO_HBASE = value.toBoolean
            case Array("--zookeeper-quorum", value: String) => Properties.ZOOKEEPER_QUORUM = value
            case Array("--namespace", value: String) => Properties.NAMESPACE = value
            case Array("--table-name-ssi", value: String) => Properties.TABLE_NAME_SDM = value
            case Array("--family-name-ssi", value: String) => Properties.FAMILY_NAME_SDM = value
            case Array("--table-name-ke", value: String) => Properties.TABLE_NAME_KE = value
            case Array("--family-name-ke", value: String) => Properties.FAMILY_NAME_KE = value
            case Array("--butch-size", value: String) => Properties.BUTCH_SIZE_TO_WRITE_HBASE = value.toInt
        }
        
    }
    
}
