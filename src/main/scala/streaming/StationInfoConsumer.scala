package streaming

import org.apache.spark.sql.{Dataset, SparkSession}
import util.StationStatusInfo

trait StationInfoConsumer {
    def getStream(spark: SparkSession, props: Map[String,String]): Dataset[StationStatusInfo]
}
