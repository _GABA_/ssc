package streaming

import org.apache.spark.sql.{Dataset, SparkSession}
import org.apache.spark.sql.functions.{col, from_json}
import util.StationStatusInfo

object JsonConsumer extends StationInfoConsumer {
    
    override def getStream(spark: SparkSession, props: Map[String, String]): Dataset[StationStatusInfo] = {
        val bootstrapServers = props("bootstrapServers")
        val topic = props("topic")
        
        import spark.implicits._
    
        val streamOfStationStatusInfo = spark.readStream.
            format("kafka").
            option("kafka.bootstrap.servers", bootstrapServers).
            option("subscribe", topic).
            load.
            selectExpr("CAST(value AS STRING)").
            select(from_json(col("value"), StationStatusInfo.getSchema).as("data")).
            select("data.*").
            as[StationStatusInfo]
        
        streamOfStationStatusInfo
    }
}
