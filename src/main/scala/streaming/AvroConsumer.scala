package streaming

import io.confluent.kafka.schemaregistry.client.{CachedSchemaRegistryClient, SchemaRegistryClient}
import io.confluent.kafka.serializers.AbstractKafkaAvroDeserializer
import org.apache.avro.Schema
import org.apache.avro.generic.GenericRecord
import util.StationStatusInfo
import org.apache.spark.sql._
import org.apache.spark.sql.avro.SchemaConverters

object AvroConsumer extends StationInfoConsumer {
    
    private var schemaRegistryClient: SchemaRegistryClient = _
    
    private var kafkaAvroDeserializer: AvroDeserializer = _
    
    def lookupTopicSchema(topic: String, isKey: Boolean = false) = {
        schemaRegistryClient.getLatestSchemaMetadata(topic + (if (isKey) "-key" else "-value")).getSchema
    }
    
    def avroSchemaToSparkSchema(avroSchema: String) = {
        SchemaConverters.toSqlType(new Schema.Parser().parse(avroSchema))
    }
    
    class AvroDeserializer extends AbstractKafkaAvroDeserializer {
        def this(client: SchemaRegistryClient) {
            this()
            this.schemaRegistry = client
        }
        
        override def deserialize(bytes: Array[Byte]): String = {
            val value = super.deserialize(bytes)
            value match {
                case str: String =>
                    str
                case _ =>
                    val genericRecord = value.asInstanceOf[GenericRecord]
                    genericRecord.toString
            }
        }
    }
    
    override def getStream(spark: SparkSession, props: Map[String, String]): Dataset[StationStatusInfo] = {
        
        val schemaRegistryUrl = props("schemaRegistryUrl")
        val bootstrapServers = props("bootstrapServers")
        val topic = props("topic")
        
        import spark.implicits._
        
        schemaRegistryClient = new CachedSchemaRegistryClient(schemaRegistryUrl, 128)
        kafkaAvroDeserializer = new AvroDeserializer(schemaRegistryClient)
        spark.udf.register("deserialize", (bytes: Array[Byte]) =>
            kafkaAvroDeserializer.deserialize(bytes)
        )
    
        val inputStream = spark.readStream
            .format("kafka")
            .option("kafka.bootstrap.servers", bootstrapServers)
            .option("subscribe", topic)
            //.option("startingOffsets", "earliest")
            .load()
    
    
        val dfValueSchema  = {
            val rawSchema = lookupTopicSchema(topic)
            avroSchemaToSparkSchema(rawSchema)
        }
    
        import org.apache.spark.sql.functions._
    
        val streamOfStationStatusInfo = inputStream.select(
            callUDF("deserialize", 'key).as("key"),
            callUDF("deserialize", 'value).as("value")
        ).
            select(from_json('value, dfValueSchema.dataType).alias("value")).
            selectExpr("value.station_id as stationId",
                "value.num_bikes_available as numBikesAvailable",
                "value.num_docks_available as numDocksAvailable",
                "value.last_reported as lastReported",
                "value.num_ebikes_available as numEbikesAvailable",
                "value.station_status as stationStatus").
            as[StationStatusInfo]
        
        streamOfStationStatusInfo
    }
}
