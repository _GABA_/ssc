package streaming

object KafkaReportStatus extends Enumeration {

    type KafkaReportStatus = Value
    
    val ZERO_BIKES_AVAILABLE = Value("bikes")
    val ZERO_EBIKES_AVAILABLE = Value("electric bikes")
    val ZERO_DOCKS_AVAILABLE = Value("docks")
    val UNAVAILABLE = Value("UNAVAILABLE")
    
}
