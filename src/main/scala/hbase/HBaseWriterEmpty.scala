package hbase

import util.StationStatusInfo

class HBaseWriterEmpty extends HBaseWriter {
    
    override def writeStationStatusInfoToHBase(sdm: StationStatusInfo): StationStatusInfo = { sdm }
    
    override def writeKafkaEventToHBase(kafkaMessage: (String, String)): (String, String) = { kafkaMessage }
    
    override def flushIfRemain(): Unit = { }
}
