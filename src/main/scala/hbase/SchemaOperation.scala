package hbase

import org.apache.hadoop.hbase.client.ConnectionFactory
import org.apache.hadoop.hbase.util.Bytes
import org.apache.hadoop.hbase.{HColumnDescriptor, HTableDescriptor, TableName}
import util.Properties

class SchemaOperation extends Serializable {
    
    def createTable(tableName: String, family: String*): Unit = {
        TableName.isLegalFullyQualifiedTableName(Bytes.toBytes(tableName))
        if(family.size < 1){
            throw new IllegalArgumentException("Table must contain at least one columnFamily")
        }
        val connection = ConnectionFactory.createConnection(Properties.HBASE_CONFIG)
        val admin = connection.getAdmin
        
        val name: String = Properties.NAMESPACE + ":" + tableName
        val table = new HTableDescriptor(name)
        for (f <- family){
            table.addFamily( new HColumnDescriptor(f))
        }
        admin.createTable(table)
        
        connection.close()
    }
    
    def renameTable(oldName: String, newName: String): Unit = {
        TableName.isLegalFullyQualifiedTableName(Bytes.toBytes(newName))
        val connection = ConnectionFactory.createConnection(Properties.HBASE_CONFIG)
        val admin = connection.getAdmin
        
        val oldTableName = TableName.valueOf(Properties.NAMESPACE + ":" + oldName)
        val newTableName = TableName.valueOf(Properties.NAMESPACE + ":" + newName)
        val snapshotName = "my_snapshot"
        
        if(admin.tableExists(oldTableName)){
            if(admin.isTableEnabled(oldTableName)){
                admin.disableTable(oldTableName)
            }
            admin.snapshot(snapshotName, oldTableName)
            admin.cloneSnapshot(snapshotName, newTableName)
            admin.deleteSnapshot(snapshotName)
            admin.deleteTable(oldTableName)
            if(admin.isTableDisabled(newTableName)){
                admin.enableTable(newTableName)
            }
        }
        
        connection.close()
    }
    
    def deleteTable(tableName: String): Unit = {
        val connection = ConnectionFactory.createConnection(Properties.HBASE_CONFIG)
        val admin = connection.getAdmin
        
        val name: TableName = TableName.valueOf(Properties.NAMESPACE + ":" + tableName)
        if(admin.tableExists(name)){
            if(admin.isTableEnabled(name)){
                admin.disableTable(name)
            }
            admin.deleteTable(name)
        }
        
        connection.close()
    }
    
    def addColumnFamilyToTable(tableName: String, familyName: String): Unit = {
        val familyList = getTableColumnFamily(tableName)
        if (familyList.contains(familyName)){
            throw new IllegalArgumentException("columnFamily with this name already exists")
        }
        
        val connection = ConnectionFactory.createConnection(Properties.HBASE_CONFIG)
        val admin = connection.getAdmin
        
        val name: String = Properties.NAMESPACE + ":" + tableName
        val table = admin.getTableDescriptor(TableName.valueOf(name))
        admin.addColumn(TableName.valueOf(name),
            new HColumnDescriptor(Bytes.toBytes(familyName)))
        
        connection.close()
    }
    
    def deleteColumnFamilyFromTable(tableName: String, familyName: String): Unit = {
        val familyList = getTableColumnFamily(tableName)
        if (familyList.size == 1){
            throw new IllegalArgumentException("Family '" + familyName +
                "' is the only column family in the table, so it cannot be deleted")
        }
        if (!familyList.contains(familyName)){
            throw new IllegalArgumentException("columnFamily with this name already not exists")
        }
        
        val connection = ConnectionFactory.createConnection(Properties.HBASE_CONFIG)
        val admin = connection.getAdmin
        
        val name: String = Properties.NAMESPACE + ":" + tableName
        admin.deleteColumn(TableName.valueOf(name),
            Bytes.toBytes(familyName))
        
        connection.close()
    }
    
    def getAllTableNames(): List[String] = {
        val connection = ConnectionFactory.createConnection(Properties.HBASE_CONFIG)
        val admin = connection.getAdmin
        
        val tablesList: List[String] = (for (td <- admin.listTables()) yield td.getNameAsString).toList
        connection.close()
        tablesList
    }
    
    def getTableColumnFamily(tableName: String): List[String] = {
        val connection = ConnectionFactory.createConnection(Properties.HBASE_CONFIG)
        val admin = connection.getAdmin
        
        val name: String = Properties.NAMESPACE + ":" + tableName
        val table = admin.getTableDescriptor(TableName.valueOf(name))
        val familyList = (for (cf <- table.getColumnFamilies) yield cf.getNameAsString).toList
        connection.close()
        familyList
    }
    
    def configOurTableInHBase(tableName: String, family: String*): Unit = {
        TableName.isLegalFullyQualifiedTableName(Bytes.toBytes(tableName))
        if(family.size < 1){
            throw new IllegalArgumentException("Table must contain at least one columnFamily")
        }
        val name: TableName = TableName.valueOf(Properties.NAMESPACE + ":" + tableName)
        val connection = ConnectionFactory.createConnection(Properties.HBASE_CONFIG)
        val admin = connection.getAdmin
        
        if(!admin.tableExists(name)){
            createTable(tableName, family(0))
        }
        val familyList = getTableColumnFamily(tableName)
        for (f <- family){
            if (!familyList.contains(f)){
                addColumnFamilyToTable(tableName, f)
            }
        }
    }
}
